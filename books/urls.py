from django.contrib import admin
from django.urls import path
from books.views import show_books, create_book ,show_book, update_view, delete_book


urlpatterns = [

    path("", show_books, name="show_books"),
    path("create/", create_book, name="create_book"),
    path("<int:pk>/", show_book, name="show_book"),
    path("<int:pk>/edit/", update_view, name="update_view"),
    path("<int:pk>/delete/",delete_book, name="delete_book"),

]
