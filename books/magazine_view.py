from django.shortcuts import get_object_or_404, redirect,  render
from books.models import Magazine, Genre ,Issue
from books.forms import MagazineForm 


def show_magazines(request):
    magazines = Magazine.objects.all()
    context = {
        "magazines" : magazines
    }

    return render(request, "magazines/magazinelist.html", context)

def show_magazine(request, pk):
    magazine = Magazine.objects.get(pk=pk)
    context = {
        "magazine" : magazine 
    }

    return render(request, "magazines/magazinedetail.html", context)

def create_magazine(request):
    context = {}

    form = MagazineForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("show_Magazines")

    context["form"] = form
    return render(request, "magazines/create_magazine.html", context)

def update_magazine(request, pk):
    # dictionary for initial data with
    # field names as keys
    context ={}
 
    # fetch the object related to passed id
    obj = get_object_or_404(Magazine, pk = pk)
 
    # pass the object as instance in form
    form = MagazineForm(request.POST or None, instance = obj)
 
    # save the data from the form and
    # redirect to detail_view
    if form.is_valid():
        form.save()
        return redirect("show_magazine", pk = pk)
 
    # add form dictionary to context
    context["form"] = form
 
    return render(request, "magazines/edit_magazine.html", context)


def delete_magazine(request, pk):
    context = {}
    # fetch the object related to passed id
    obj = get_object_or_404(Magazine, pk = pk)
    if request.method =="POST":
        # delete object
        obj.delete()
        # after deleting redirect to
        # home page
        return redirect("show_magazines")
    
 
    return render(request, "magazines/delete_magazine.html", context) 


def genre_list(request, pk):
    current_genre = Genre.objects.get(pk = pk)
    context = {
        "genre" : current_genre
    }

    return render(request, "magazines/genre_list.html", context)



def issue_list(request):
    issues = Issue.objects.all()
    context = {
        "issue" : issues
    }

    return render(request, "magazines/issue_list.html", context)

