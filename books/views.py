from django.shortcuts import get_object_or_404, redirect,  render
from books.forms import BookForm 
from books.models import Book 


# Create your views here.


def show_books(request):
    books = Book.objects.all()
    context = {
        "books" : books
    }

    return render(request, "books/list.html", context)


def show_book(request, pk):
    book = Book.objects.get(pk=pk) 
    context = {
        "book" : book 

    }
    return render(request, "books/details.html", context)


def create_book(request):
    context = {}

    form = BookForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("show_books")
    
    context["form"] = form
    return render(request, "books/create.html", context)



# this works also     
# def update_view(request, pk):
#     if Book and BookForm:
#         instance = Book.objects.get(pk=pk)
#         if request.method == "POST":
#             form = BookForm(request.POST, instance=instance)
#             if form.is_valid():
#                 form.save()
#                 return redirect("show_books")
#         else:
#             form = BookForm(instance=instance)
#     else:
#         form = None
#     context = {
#         "form": form,
#     }
#     return render(request, "books/edit.html", context)


def update_view(request, pk):
    # dictionary for initial data with
    # field names as keys
    context ={}
 
    # fetch the object related to passed id
    obj = get_object_or_404(Book, pk = pk)
 
    # pass the object as instance in form
    form = BookForm(request.POST or None, instance = obj)
 
    # save the data from the form and
    # redirect to detail_view
    if form.is_valid():
        form.save()
        return redirect("show_book", pk = pk)
 
    # add form dictionary to context
    context["form"] = form
 
    return render(request, "books/edit.html", context)


def delete_book(request, pk):
    context = {}
    # fetch the object related to passed id
    obj = get_object_or_404(Book, pk = pk)
    if request.method =="POST":
        # delete object
        obj.delete()
        # after deleting redirect to
        # home page
        return redirect("show_books")
 
    return render(request, "books/delete.html", context) 


