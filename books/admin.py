from django.contrib import admin
from books.models import Book, Magazine, Author, BookReview, Genre , Issue

# Register your models here.

# class BookAdmin(admin.ModelAdmin):
#     pass

admin.site.register(Book)
admin.site.register(Magazine)
admin.site.register(BookReview)
admin.site.register(Author)
admin.site.register(Genre)
admin.site.register(Issue)

