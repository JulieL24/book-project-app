from django.db import models
from django.contrib.auth.models import User

# Create your models here.
# class is a blueprint which you create an object

class  Author(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name


class Book(models.Model): #class is blueprint
    title = models.CharField(max_length=200, unique=True)
    author = models.ManyToManyField(Author, related_name="books",blank=True)    
    pages = models.SmallIntegerField(null=True)
    isbn = models.BigIntegerField(null=True)
    cover = models.URLField(null=True, blank=True)
    description = models.TextField(null=True)
    in_print = models.BooleanField(null=True)
    publish_year = models.SmallIntegerField(null=True)


    # if you want to represent an instance of this model class as a string
    def __str__(self):
        return self.title + " by " + str(self.author.first())


class  BookReview(models.Model):
    reviewer = models.ForeignKey(User, related_name="reviews", on_delete=models.CASCADE)
    book = models.ForeignKey(Book, related_name="reviews", on_delete=models.CASCADE)
    text = models.TextField()


class Genre(models.Model):
    name = models.CharField(max_length=200,unique=True)

    def __str__(self):
        return self.name 


class Magazine(models.Model):
    title = models.CharField(max_length=200,unique=True)
    cover_image = models.URLField(null=True, blank=True)
    release_cycle = models.CharField(max_length=200,null=True)
    description = models.TextField(null=True)
    genres = models.ManyToManyField(Genre, related_name = "magazines", blank=True)

    def __str__(self):
        return self.title


class Issue(models.Model):
    magazine = models.ForeignKey(Magazine, related_name="issues", on_delete=models.CASCADE,null=True)
    title = models.CharField(max_length=200,unique=True)
    description = models.TextField(null=True)
    cover_image = models.URLField(null=True,blank=True)
    page_count = models.SmallIntegerField(null=True)
    issue_number = models.SmallIntegerField(null=True, blank=True)
    published_date = models.SmallIntegerField(null=True)
    
    def __str__(self):
        return self.title