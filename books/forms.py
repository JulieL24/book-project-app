from django import forms
from books.models import Book, Magazine

class BookForm(forms.ModelForm):
    class Meta:
        model = Book


        fields = [
            "title",
            "author",
            "pages",
            "isbn",
            "cover",
            "in_print",
            "description",
            "publish_year",

        ]

class MagazineForm(forms.ModelForm):
    class Meta:
        model = Magazine

      
        fields = "__all__"
  
