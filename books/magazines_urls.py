from django.contrib import admin
from django.urls import path
from books.magazine_view import show_magazines, show_magazine, create_magazine, update_magazine, delete_magazine, genre_list, issue_list


urlpatterns = [


    path("", show_magazines, name ="show_magazines"),
    path("<int:pk>/", show_magazine, name="show_magazine"),
    path("create/", create_magazine, name="create_magazine"),
    path("<int:pk>/edit/", update_magazine, name="update_magazine"),
    path("<int:pk>/delete/", delete_magazine, name="delete_magazine"),
    path("genre/<int:pk>/", genre_list, name="genre_list"),
    path("issue/", issue_list, name="issue_list"),

]

